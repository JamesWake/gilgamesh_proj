﻿using UnityEngine;
using System.Collections;

public class BreadController : MonoBehaviour
{
	//Flag representing whether the bread is targeted
	private bool targeted;

	//Called on initialization
	void Awake()
	{
		//Initialize BreadController
		targeted = false;
	}

	//Returns whether the piece of bread is targeted
	public bool IsTargeted()
	{
		//Return the targeted flag
		return targeted;
	}

	//Sets the flag representing whether the piece of bread is targeted
	public void SetTargeted(bool arg)
	{
		//Set the targeted flag to the specified argument
		targeted = arg;
	}
}
