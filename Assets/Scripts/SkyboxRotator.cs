﻿using UnityEngine;
using System.Collections;

public class SkyboxRotator : MonoBehaviour {
    float rotVal;
    public float speed;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (rotVal >= 360)
        {
            rotVal = 0;
        }
        rotVal += Time.deltaTime * speed;
        RenderSettings.skybox.SetFloat("_Rotation", rotVal);
	}
}