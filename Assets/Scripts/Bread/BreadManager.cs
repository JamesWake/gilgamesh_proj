﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BreadManager : MonoBehaviour
{
	//The prefab to use as bread
	public GameObject breadPrefab;

	//The DuckManager
	public DuckManager duckManager;

    //GameObjects that destroy bread when bread collide with them
    public GameObject[] destroyBreadGameObjects;

    //The players bread list
    private List<GameObject> breadList;

	//Called on initialization
	void Awake()
	{
		//Initialize BreadManager
		breadList = new List<GameObject>();
	}

	//Called once per frame
	void Update()
	{
		//For each piece of bread
		for (int breadIndex = 0; breadIndex < breadList.Count; breadIndex++)
		{
			//If the bread does not exist
			if (breadList[breadIndex] == null)
			{
				//Remove the bread from the bread list
				breadList.RemoveAt(breadIndex);

				//Decrement the bread index
				breadIndex--;

				//Continue to the next piece of bread
				continue;
			}

			//If the piece of bread has not already been targeted
			if(!breadList[breadIndex].GetComponent<BreadController>().IsTargeted())
			{
				//Get the closest to the bread non busy duck that has found the player
				GameObject closestDuck = duckManager.GetClosestFoundNonBusyDuck(breadList[breadIndex].transform.position);

				//If the closest duck is valid
				if(closestDuck != null)
				{
					//Get the ducks DuckController
					DuckController duckController = closestDuck.GetComponent<DuckController>();

					//Make the closest duck eat the bread
					duckController.SetBehaviour(new EatBehaviour(closestDuck, breadList[breadIndex], duckController.breadFollowDistance, duckController.breadEatTime));

					//Make the closest duck quack
					duckController.Quack();

					//Set the piece of bread as targeted
					breadList[breadIndex].GetComponent<BreadController>().SetTargeted(true);
				}
			}
            //For each Destroy bread GameObject
            for (int breadDestroyerIndex = 0; breadDestroyerIndex < destroyBreadGameObjects.Length; breadDestroyerIndex++)
            {
                //If the bread destroyer object is enabled
                if (destroyBreadGameObjects[breadDestroyerIndex].activeInHierarchy)
                {
                    //If the bread is intersecting the bread destroyer GameObject
                    if (AABB(destroyBreadGameObjects[breadDestroyerIndex].transform.position,
                            destroyBreadGameObjects[breadDestroyerIndex].transform.lossyScale,
                            breadList[breadIndex].transform.position,
                            breadList[breadIndex].transform.lossyScale))
                    {
                        //Destroy the piece of bread
                        Destroyer.DestroyGameObject(breadList[breadIndex]);
                    }
                }
            }
        }
	}

	//Spawns a piece of bread at the specified position
	//If there is already the maximum allowed number of bread spawned the first piece of bread is destroyed in order to allow a new piece of bread to spawn
	public void SpawnBread(Vector3 position, Quaternion rotation)
	{
		//If there is less then one duck found
		if(duckManager.GetNumFoundDucks() < 1)
		{
			//Return from the function
			return;
		}

		//If there is three or more bread in the players bread list
		if (breadList.Count >= duckManager.GetNumFoundDucks())
		{
			//Destroy the first piece of bread in the bread list
			Destroy(breadList[0]);

			//Remove the first piece of bread in the bread list
			breadList.RemoveAt(0);
		}

		//Spawn the bread prefab with the targets position and rotation
		breadList.Add((GameObject)Instantiate(breadPrefab, position, rotation, null));
	}

    //Returns true if the points intersect collide
    public bool AABB(Vector3 position1, Vector3 scale1, Vector3 position2, Vector3 scale2)
    {
        //Modify the scales
        scale1 /= 2.0f;
        scale2 /= 2.0f;

        //Return whether the points intersect
        return (position1.x - scale1.x <= position2.x + scale2.x && position1.x + scale1.x >= position2.x - scale2.x) &&
               (position1.y - scale1.y <= position2.y + scale2.y && position1.y + scale1.y >= position2.y - scale2.y) &&
               (position1.z - scale1.z <= position2.z + scale2.z && position1.z + scale1.z >= position2.z - scale2.z);
    }
}
