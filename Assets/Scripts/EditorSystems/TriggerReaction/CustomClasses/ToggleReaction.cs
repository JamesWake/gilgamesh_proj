﻿using UnityEngine;
using System.Collections;

public class ToggleReaction : EditorReaction
{
	//The GameObject to toggle activeness
	public GameObject toggleGameObject;

	//Called when the trigger this reaction is linked to is triggered
	public override void React()
	{
		//Toggle the GameObjects activeness
		toggleGameObject.SetActive(!toggleGameObject.activeInHierarchy);
	}
}
