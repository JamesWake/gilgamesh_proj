﻿using UnityEngine;
using System.Collections;
/// <summary>
/// James
/// Base class to all scripts, for game wide useful code ie component referencing/math overloading
/// </summary>
public class SuperBehaviour : MonoBehaviour {
    protected AudioSource audioSource;
    protected NavMeshAgent navAgent;
    public static Vector3 up = Vector3.up, left = Vector3.left, right = Vector3.right, down = Vector3.down;
    void Awake ()
    {
        audioSource = GetComponent<AudioSource>();
        navAgent = GetComponent<NavMeshAgent>();
    }

    public static Vector3 ReturnFlatVec (Vector3 i)
    {
        return new Vector3(i.x, 0, i.y);
    }
}
