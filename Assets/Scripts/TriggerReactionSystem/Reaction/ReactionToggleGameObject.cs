﻿using UnityEngine;
using System.Collections;

public class ReactionToggleGameObject : Reaction {
    public GameObject toggleThisGameObject;
    public override void Fire(GameObject objectToCheck)
    {
        base.Fire(objectToCheck);
        toggleThisGameObject.SetActive(!toggleThisGameObject.activeInHierarchy);
    }
}
