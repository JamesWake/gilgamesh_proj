﻿using UnityEngine;
using System.Collections;

public class RoadExit : MonoBehaviour {

    public bool DuckInside = false;
    public bool PlayerInside = false;
    public GameObject Toggle;
    public GameObject Toggle1;
	
	// Update is called once per frame
	void Update () {
	    if(DuckInside && PlayerInside)
        {
            Toggle.SetActive(false);
            Toggle1.SetActive(true);
            enabled = false;
        }
	}

    void OnTriggerEnter(Collider Col)
    {
        if(Col.gameObject.tag == "Duck")
        {
            DuckInside = true;
        }
        if (Col.gameObject.tag == "Player")
        {
            PlayerInside = true;
        }
    }

    void OnTriggerExit(Collider Col)
    {
        if (Col.gameObject.tag == "Duck")
        {
            DuckInside = false;
        }
        if (Col.gameObject.tag == "Player")
        {
            PlayerInside = false;
        }
    }
}
