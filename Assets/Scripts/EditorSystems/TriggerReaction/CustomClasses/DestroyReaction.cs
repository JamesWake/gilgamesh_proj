﻿using UnityEngine;
using System.Collections;

public class DestroyReaction : EditorReaction
{
	//The GameObject to destroy
	public GameObject destroyGameObject;

	//Called when the trigger this reaction is linkeed to is triggered
	public override void React()
	{
		//Destroy the GameObject
		Destroyer.DestroyGameObject(destroyGameObject);
	}
}
