﻿using UnityEngine;
using System.Collections;

public class AudioPlayer
{
    //The AudioPlayers AudioSource
    private AudioSource audioSource = null;

    //The type of AudioPlayer this AudioPlayer is (ie. SoundEffect Player, BackgroundMusic Player)
    private ushort audioPlayerType = 0;

    //Creates a AudioSource component for the AudioPlayer
    //If the AudioPlayer already has a valid AudioSource the function Logs a warning
    public void CreateAudioSource(GameObject owner)
    {
        //If the AudioPlayers AudioSource is not valid
        if(audioSource == null)
        {
            //Create an AudioSource for the AudioPlayer
            audioSource = owner.AddComponent<AudioSource>();

            //Default configure the AudioPlayers AudioSource
            audioSource.playOnAwake = false;
        }
        //Otherwise
        else
        {
            //Debug log an warning
            Debug.LogWarning("The AudioPlayer you attempted to create an AudioSource for already has an AudioSource!");
        }
    }

    //Returns the AudioPlayers AudioSource
    public AudioSource GetAudioSource()
    {
        //Return the AudioPlayers AudioSource
        return audioSource;
    }

    //Returns the type of AudioPlayer the AudioPlayer is
    public ushort GetPlayerType()
    {
        //Return the AudioPlayers audio player type
        return audioPlayerType;
    }

    //Returns the mode of the AudioPlayer
    public ushort GetPlayingMode()
    {
        //If the AudioPlayer has an AudioSource
        if (audioSource != null)
        {
            //If the AudioPlayers AudioSource is playing sound
            if (audioSource.isPlaying)
            {
                //Return the AudioPlayerMode Playing
                return AudioPlayerMode.Playing;
            }
            //Otherwise
            else
            {
                //Return the AudioPlayerMode Standby
                return AudioPlayerMode.Standby;
            }
        }
        //Otherwise
        else
        {
            //Return the AudioPlayerMode Invalid
            return AudioPlayerMode.Invalid;
        }
    }

    //Forces the AudioPlayer to start playing
    //If the AudioPlayer doesn't have a valid AudioSource an error is logged
    //If the AudioPlayers AudioSource doesn't have a valid AudioClip an error is logged
    public void Play()
    {
        //If the AudioPlayer has a valid AudioSource
        if (audioSource != null)
        {
            //If the AudioPlayer's AudioSource's AudioClip is valid
            if (audioSource.clip != null)
            {
                //Begin playing the AudioClip
                audioSource.Play();
            }
            //Otherwise
            else
            {
                //Debug log an error
                Debug.LogError("AudioPlayer's AudioSource's must have a valid AudioClip before playing!");
            }
        }
        //Otherwise
        else
        {
            //Debug log an error
            Debug.LogError("AudioPlayer's must have a valid AudioSource before playing!");
        }
    }

    //Sets the AudioPlayers AudioClip (The music file to play)
    //If the AudioPlayer does not have a valid AudioSource the function Logs an error
    public void SetAudioClip(AudioClip audioClip)
    {
        //If the AudioClip has a valid AudioSource
        if(audioSource != null)
        {
            //Set the AudioPlayers AudioSources AudioClip
            audioSource.clip = audioClip;
        }
        //Otherwise
        else
        {
            //Debug log an error
            Debug.LogError("AudioPlayer's must have a valid AudioSource before setting their AudioClip!");
        }
    }

    //Sets the AudioPlayers player type
    public void SetPlayerType(ushort playerType)
    {
        //Set the AudioPlayers type to the specified player type
        audioPlayerType = playerType;
    }

    //Sets the AudioPlayers playing mode
    public void SetPlayingMode(ushort playingMode)
    {
        //If the specified playing mode is Once
        if (playingMode == AudioPlayingMode.Once)
        {
            //Set the AudioSources loop flag to false
            audioSource.loop = false;
        }
        //Otherwise if the specified playing mode is looping
        else if (playingMode == AudioPlayingMode.Looping)
        {
            //Set the AudioSources loop flag to true
            audioSource.loop = true;
        }
    }

    //Sets the volume the AudioPlayer is playing at (Range(0.0f, 1.0f)
    public void SetVolume(float volume)
    {
        //Set the AudioPlayer's AudioSource's volume to the specified volume
        audioSource.volume = volume;
    }
}
