﻿using UnityEngine;
using System.Collections;

public class StayBehaviour : Behaviour
{
    //The location to stay at
    private Vector3 location;

    //The distance away from the location the GameObject with this script attached can be and still be considered at the location
    private float stayDistance;

	//The AI's NavMeshAgent
	private NavMeshAgent navMeshAgent;

	//The AI's path
	private NavMeshPath navMeshPath;

	//The AI's duck controller
	private DuckController duckController;

    //Creates and initializes a StayBehaviour instance
    public StayBehaviour(GameObject owner = null, Vector3 location = new Vector3(), float stayDistance = 1.0f)
    {
        //Initialize StayBehaviour
        SetOwner(owner);
        SetLocation(location);
        SetStayDistance(stayDistance);
		navMeshAgent = owner.GetComponent<NavMeshAgent>();
		navMeshAgent.Stop();
		duckController = owner.GetComponent<DuckController>();
	}

    //Performs the operations necessary for the stay behaviour
    public override void Behave()
    {
        //If the owner GameObject exists
        if(owner != null)
        {
			//Disable automatic AI movement from the AI's nav mesh agent
			navMeshAgent.Stop();

            //If the owner GameObject is outside of the stay distance
            if(Vector3.Distance(owner.transform.position, location) > stayDistance)
            {
				//Set the AI's destination to the AI's stay location
				navMeshAgent.SetDestination(location);

				//If the AI is not already at it's destination
				if (navMeshAgent.path.corners.Length > 1)
				{
					//Rotate to look at the next corner in the AI's path
					duckController.LookAtLocation(navMeshAgent.path.corners[1]);

					//Move the AI Forward
					duckController.AddForwardForce();
				}
			}
        }
    }

    //Sets the location the GameObject this script is attached should stay around
    public void SetLocation(Vector3 arg)
    {
        //Set the location to the specified argument
        location = arg;
    }

    //Sets the stay distance (The distance away from the stay location the GameObject with this script attached can be whilst still being considered at the stay location)
    public void SetStayDistance(float arg)
    {
        //Set the stay distance to the specified argument
        stayDistance = arg;
    }
}
