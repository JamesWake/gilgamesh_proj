﻿using UnityEngine;
using System.Collections;

public class ReactionDebugLog : Reaction {
    public string logThis = "Test";

    override public void Fire (GameObject objectToCheck)
    {
        base.Fire(objectToCheck);
        Debug.Log(logThis, this);
    }

}
