﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class ReactionPlayAudioClip : Reaction {
    
    [Header("Randombly plays one of these clips")]
    public List<AudioClip> clips;

    public bool twoDimentional = false;

    public override void Fire(GameObject objectToCheck)
    {
        base.Fire(objectToCheck);
        if (clips.Count != 0)
        {
            if (twoDimentional)
            {
                AudioManager.PlayAudio(clips[Random.Range(0, clips.Count - 1)], AudioPlayerType.Effect, AudioPlayingMode.Once);
            } else
            {
                AudioManager.PlayAudio(clips[Random.Range(0, clips.Count - 1)], gameObject, AudioPlayerType.Effect, AudioPlayingMode.Once);
            }
        }
        else
            Debug.LogWarning("No audio clips in list", this);
    }
}
