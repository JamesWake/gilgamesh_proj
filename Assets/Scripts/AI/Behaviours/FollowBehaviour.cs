﻿using UnityEngine;
using System.Collections;

public class FollowBehaviour : Behaviour
{
	//The GameObject to follow
	private GameObject target;

	//The Distance away from the target GameObject that required the GameObject with this script attached to move closer to the target GameObject
	private float followDistance;

	//The AI's NavMeshAgent
	private NavMeshAgent navMeshAgent;

	//The AI's duck controller
	private DuckController duckController;

	//Creates and initializes a FollowBehaviour instance
	public FollowBehaviour(GameObject owner = null, GameObject target = null, float followDistance = 1.0f)
	{
		//Initialize FollowBehaviour
		SetOwner(owner);
		SetTarget(target);
		SetFollowDistance(followDistance);
		navMeshAgent = owner.GetComponent<NavMeshAgent>();
		navMeshAgent.Stop();
		duckController = owner.GetComponent<DuckController>();
	}

	//Performs the operations necessary for the follow behaviour
	public override void Behave()
	{
		//If the owner GameObject exists
		if (owner != null)
		{
			//Disable automatic AI movement from the AI's nav mesh agent
			navMeshAgent.Stop();

			//If the target GameObject exists
			if (target != null)
			{
				//If the GameObject with this script attached is further away from the target GameObject then the follow distance
				if (Vector3.Distance(GetModifiedPosition(), GetTargetModifedPosition()) > followDistance)
				{
					//Set the AI's destination to the AI's stay location
					navMeshAgent.SetDestination(target.transform.position);

                    //If the AI is not already at it's destination
                    if (navMeshAgent.path.corners.Length > 1)
					{
						//Rotate to look at the next corner in the AI's path
						duckController.LookAtLocation(navMeshAgent.path.corners[1]);

						//Move the AI Forward
						duckController.AddForwardForce();
                    }
				}
			}
		}
	}

	//Sets the target GameObject (The object to follow)
	public void SetTarget(GameObject arg)
	{
		//Set the target GameObject to the specified GameObject
		target = arg;
	}

	//Sets the follow distance (The maximum distance the GameObject with this script attached can be from it's target GameObject before moving closer to it's target GameObject)
	public void SetFollowDistance(float arg)
	{
		//Set the follow distance to the specified argument
		followDistance = arg;
	}

	//Returns the owner GameObjects modified position (2D [x, z])
	private Vector3 GetModifiedPosition()
	{
		//Return the owner GameObjects 2D position using the x and z axis
		return new Vector3(owner.transform.position.x, 0.0f, owner.transform.position.z);
	}

	//Returns the target GameObjects modified position (2D [x, z])
	private Vector3 GetTargetModifedPosition()
	{
		//Return the target GameObject 2D position using the x and z axis
		return new Vector3(target.transform.position.x, 0.0f, target.transform.position.z);
	}
}
