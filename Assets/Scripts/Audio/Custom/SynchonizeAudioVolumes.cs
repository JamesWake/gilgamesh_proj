﻿using UnityEngine;
using System.Collections;

public class SynchonizeAudioVolumes : MonoBehaviour
{
	//The effect volume slider
	public UI_Slider effectSlider;

	//The background music slider
	public UI_Slider backgroundMusicSlider;

	//Called once per frame
	void Update()
	{
		//Synchronize volume levels
		AudioManager.SetEffectsVolume(effectSlider.EvaluateHorizontal());
		AudioManager.SetBackgroundMusicVolume(backgroundMusicSlider.EvaluateHorizontal());
	}
}
