﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class DuckManager : MonoBehaviour
{
	//Player GameObject
	public GameObject player;

	//List of Duck's
	public GameObject[] duckList;

	//The sound the player makes when discovering a duck
	public AudioClip[] playerDiscoveryAudio;

	//The index for the player discovery audio
	private int playerDiscoveryAudioIndex = 0;

	//List of Duck's duck controllers
	private DuckController[] duckControllerList;

	//List of boolean values representing whether a duck has found the player
	private bool[] duckFoundPlayer;

	//Called on initialization
	void Awake()
	{
		//Initialize DuckManager
		duckControllerList = new DuckController[duckList.Length];
		duckFoundPlayer = new bool[duckList.Length];
		for (int index = 0; index < duckList.Length; index++)
		{
			duckControllerList[index] = duckList[index].GetComponent<DuckController>();
			duckFoundPlayer[index] = false;
		}
	}

	//Called once per frame
	void Update()
	{
		//Create sorted list for sorting the order the available ducks should follow
		SortedList<float, GameObject> orderedDuckList = new SortedList<float, GameObject>();

		//For each duck in the duck list
		for (int duck = 0; duck < duckList.Length; duck++)
		{
			//If the duck does not exist
			if(duckControllerList[duck] == null)
			{
				//Continue to the next duck
				continue;
			}

			//If the duck is busy
			if (duckControllerList[duck].IsBusy())
			{
				//Continue to the next duck
				continue;
			}

			//If the duck has not found the player
			if (!duckFoundPlayer[duck])
			{
				//Set the duck found player flag for the duck to whether the duck can see the player
				duckFoundPlayer[duck] = CanDuckSeePlayer(duckList[duck]);

				//If the duck can see the player now
				if (duckFoundPlayer[duck])
				{
					//Make the duck quack
					duckControllerList[duck].Quack();

					//Play Gilgamesh discovery duck audio
					AudioManager.PlayAudio(playerDiscoveryAudio[playerDiscoveryAudioIndex], AudioPlayerType.Effect, AudioPlayingMode.Once);

					//Increment the player discovery audio index
					playerDiscoveryAudioIndex++;

					//If the player discovery audio index is past the last audio index
					if(playerDiscoveryAudioIndex > playerDiscoveryAudio.Length - 1)
					{
						//Reset the player discovery audio index
						playerDiscoveryAudioIndex = 0;
					}
				}

				//Continue to the next duck
				continue;
			}

			//Add the duck to the ordered duck list
			orderedDuckList.Add(Vector3.Distance(duckList[duck].transform.position, player.transform.position), duckList[duck]);
		}

		//If there is atleast one duck in the oreder duck list
		if (orderedDuckList.Count > 0)
		{
			//Create GameObject for storing the duck
			GameObject duck = null;

			//Get the duck GameObject
			orderedDuckList.TryGetValue(orderedDuckList.Keys[0], out duck);

			//If the duck was retrieved successfully
			if (duck != null)
			{
				//Get the ducks DuckController
				DuckController duckController = duck.GetComponent<DuckController>();

				//Make the duck closest to the player follow the player
				duckController.SetBehaviour(new FollowBehaviour(duck, player, duckController.playerFollowDistance));
			}
		}

		//For each duck in the ordered duck list that should follow a different duck
		for (int duckIndex = 1; duckIndex < orderedDuckList.Count; duckIndex++)
		{
			//Create GameObject for storing the duck
			GameObject duck = null;

			//Create GameObject for storing the previous duck
			GameObject previousDuck = null;

			//Get the duck GameObject
			orderedDuckList.TryGetValue(orderedDuckList.Keys[duckIndex], out duck);

			//Get the previous duck GameObject
			orderedDuckList.TryGetValue(orderedDuckList.Keys[duckIndex - 1], out previousDuck);

			//If the duck were retrieved successfully
			if (duck != null && previousDuck != null)
			{
				//Get the ducks DuckController
				DuckController duckController = duck.GetComponent<DuckController>();

				//Make the duck follow the previous duck
				duck.GetComponent<DuckController>().SetBehaviour(new FollowBehaviour(duck, previousDuck, duckController.duckFollowDistance));
			}
		}
	}

	//Returns true if the specified duck can see the player
	private bool CanDuckSeePlayer(GameObject duck)
	{
		//If the player is within the ducks look distance
		if (Vector3.Distance(duck.transform.position, player.transform.position) <= duck.GetComponent<DuckController>().duckLookDistance)
		{
			//Create Raycast hit object
			RaycastHit hit;

			//If the duck can see the player
			if (Physics.Linecast(duck.transform.position, player.transform.position, out hit))
			{
				//If the object the duck is looking at is the player
				if (hit.transform.tag == player.tag)
				{
					//The duck can see the player
					return true;
				}
			}
		}

		//The duck cannot see the player
		return false;
	}

	//Returns the closest non busy duck GameObject that has been found
	public GameObject GetClosestFoundNonBusyDuck(Vector3 position)
	{
		//Closest distance
		float closestDistance = float.MaxValue;

		//Index of closest duck
		int closestDuckIndex = -1;

		//For each duck
		for (int duckIndex = 0; duckIndex < duckList.Length; duckIndex++)
		{
			//If the duck does not exist
			if(duckControllerList[duckIndex] == null)
			{
				//Continue to the next duck
				continue;
			}

			//If the duck is not busy or not found
			if (duckControllerList[duckIndex].IsBusy() || !duckFoundPlayer[duckIndex])
			{
				//Continue to the next duck
				continue;
			}

			//Calculate the distance from the duck to the specified position
			float distance = Vector3.Distance(duckList[duckIndex].transform.position, position);

			//If the duck is closer to the specified position then the closest distance
			if (distance < closestDistance)
			{
				//Set the closest distance to the distance
				closestDistance = distance;

				//Set the closest duck index to the current duck index
				closestDuckIndex = duckIndex;
			}
		}

		//If the closest duck index is more then -1
		if (closestDuckIndex > -1)
		{
			//Return the closest duck GameObject
			return duckList[closestDuckIndex];
		}

		//All ducks are busy or there are no ducks
		return null;
	}

	//Returns the GameObject closests to the specified target GameObject
	private GameObject GetClosestGameObject(GameObject targetGameObject, List<GameObject> gameObjectList)
	{
		//Closest distance
		float closestDistance = float.MaxValue;

		//Index of closest GameObject
		int closestIndex = 0;

		//For each GameObject in the GameObject list
		for (int gameObjectIndex = 0; gameObjectIndex < gameObjectList.Count; gameObjectIndex++)
		{
			//If the GameObject does not exist
			if(gameObjectList[gameObjectIndex] == null)
			{
				//Continue to the next GameObject
				continue;
			}

			//Calculate the distance from the GameObject to the target GameObject
			float distance = Vector3.Distance(gameObjectList[gameObjectIndex].transform.position, targetGameObject.transform.position);

			//If the duck is closer to the specified position then the closest distance
			if (distance < closestDistance)
			{
				//Set the closest distance to the distance
				closestDistance = distance;

				//Set the closest index to the current gameObject index
				closestIndex = gameObjectIndex;
			}
		}

		//Return the closest GameObject
		return gameObjectList[closestIndex];
	}

	//Returns the number of ducks that have been found
	public int GetNumFoundDucks()
	{
		//The number of ducks that have been found
		int ducksFound = 0;

		//For each flag in the duck found player list
		for(int flagIndex = 0; flagIndex < duckFoundPlayer.Length; flagIndex++)
		{
			//If the duck has been found and exists
			if(duckFoundPlayer[flagIndex] && duckList[flagIndex] != null)
			{
				//Increment the number of ducks found
				ducksFound++;
			}
		}

		//Return the number of ducks that have been found
		return ducksFound;
	}
}
