﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Collider))]
public class TriggerColliderEnter : Trigger {

    void OnCollisionEnter(Collision collision)
    {
        Fire(collision.gameObject);
    }
}
