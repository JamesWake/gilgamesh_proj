﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Manages the creation and deletion of AudioPlayers
public class AudioManager : MonoBehaviour
{
    //The master volume
    [Range(0.0f, 1.0f)]
    public static float masterVolume = 1.0f;

    //The effects volume
    [Range(0.0f, 1.0f)]
    public static float effectVolume = 1.0f;

    //The background music volume
    [Range(0.0f, 1.0f)]
    public static float backgroundMusicVolume = 1.0f;

    //List of AudioPlayers
    private static List<AudioPlayer> audioPlayerList = new List<AudioPlayer>();

    //Called at the end of each frame
    void Update()
    {
        //Remove all AudioPlayers not playing sound
        RemoveAllStandbyAudioPlayers();

        //Synchronize all AudioPlayers volumes
        SynchronizeAudioPlayerVolumes();
    }

    //Removes all AudioPlayers in not playing any sound in the AudioPlayer list
    public static void RemoveAllStandbyAudioPlayers()
    {
        //For each AudioPlayer in the AudioPlayer list
        for (int audioPlayerIndex = 0; audioPlayerIndex < audioPlayerList.Count; audioPlayerIndex++)
        {
            //If the AudioPlayer is not playing sound
            if (audioPlayerList[audioPlayerIndex].GetPlayingMode() != AudioPlayerMode.Playing)
            {
                //Destroy the AudioPlayers AudioSource on the GameObject it is attached to
                DestroyObject(audioPlayerList[audioPlayerIndex].GetAudioSource());

                //Remove the AudioPlayer from the AudioPlayer list
                audioPlayerList.RemoveAt(audioPlayerIndex);

                //Decrement the audio player index so the next audio player is not skipped
                audioPlayerIndex--;
            }
        }
    }

    //Synchronizes all the volumes for the AudioPlayers in the AudioPlayer list
    public static void SynchronizeAudioPlayerVolumes()
    {
        //For each AudioPlayer in the AudioPlayer list
        for(int audioPlayerIndex = 0; audioPlayerIndex < audioPlayerList.Count; audioPlayerIndex++)
        {
            //Calculate the volume to set the AudioPlayer based of it's PlayerType
            audioPlayerList[audioPlayerIndex].SetVolume(CalculateVolumeForPlayerType(audioPlayerList[audioPlayerIndex].GetPlayerType()));
        }
    }

    //Returns the appropriate volume for the specified AudioPlayerType
    public static float CalculateVolumeForPlayerType(ushort audioPlayerType)
    {
        //Depending on the AudioPlayerType
        switch (audioPlayerType)
        {
            //Return the appropriate volume for Standard audio
            case AudioPlayerType.Standard: return masterVolume;

            //Return the appropriate volume for effects
            case AudioPlayerType.Effect: return effectVolume * masterVolume;

            //Return the appropriate volume for Background music
            case AudioPlayerType.BackgroundMusic: return backgroundMusicVolume * masterVolume;
        }

        //Suppress warnings (Volume for unknown AudioPlayerType)
        return 0.0f;
    }

	//Plays the specified AudioClip once with the applicable settings
	//audioClip - The music file to play
	//audioPlayerType - The type of audio player to create (Standard, Effect, Background Music)
	//audioPlayingMode - Whether to play the AudioClip once or play it looping
	public static void PlayAudio(AudioClip audioClip, ushort audioPlayerType, ushort audioPlayingMode)
	{
		//Create AudioPlayer
		AudioPlayer audioPlayer = new AudioPlayer();

		//Initialize AudioPlayer
		audioPlayer.CreateAudioSource(Camera.main.gameObject);

		//Configure AudioPlayer
		audioPlayer.SetAudioClip(audioClip);
		audioPlayer.SetPlayerType(audioPlayerType);
		audioPlayer.SetPlayingMode(audioPlayingMode);

		//Set the starting volume for the AudioPlayer
		audioPlayer.SetVolume(CalculateVolumeForPlayerType(audioPlayerType));

		//Add the AudioPlayer to the AudioPlayer list
		audioPlayerList.Add(audioPlayer);

		//Start the AudioPlayer
		audioPlayer.Play();
	}

	//Plays the specified AudioClip once with the applicable settings
	//audioClip - The music file to play
	//audioOwner - The owning GameObject of the audio (3D sound)
	//audioPlayerType - The type of audio player to create (Standard, Effect, Background Music)
	//audioPlayingMode - Whether to play the AudioClip once or play it looping
	public static void PlayAudio(AudioClip audioClip, GameObject audioOwner, ushort audioPlayerType, ushort audioPlayingMode)
    {
        //Create AudioPlayer
        AudioPlayer audioPlayer = new AudioPlayer();

        //Initialize AudioPlayer
        audioPlayer.CreateAudioSource(audioOwner);

        //Configure AudioPlayer
        audioPlayer.SetAudioClip(audioClip);
        audioPlayer.SetPlayerType(audioPlayerType);
        audioPlayer.SetPlayingMode(audioPlayingMode);

        //Set the starting volume for the AudioPlayer
        audioPlayer.SetVolume(CalculateVolumeForPlayerType(audioPlayerType));

		//Set the audio players playing mode to 3D
		audioPlayer.GetAudioSource().spatialBlend = 1.0f;

		//Add the AudioPlayer to the AudioPlayer list
		audioPlayerList.Add(audioPlayer);

        //Start the AudioPlayer
        audioPlayer.Play();
    }

	//Sets the Master volume
	public static void SetMasterVolume(float newMasterVolume)
	{
		//Set the master volume to the specfied new master volume
		masterVolume = newMasterVolume;
	}

	//Sets the Effects volume
	public static void SetEffectsVolume(float newEffectsVolume)
	{
		//Set the Effects volume to the specified new effects volume
		effectVolume = newEffectsVolume;
	}

	//Sets the Background music volume
	public static void SetBackgroundMusicVolume(float newBackgroundMusicVolume)
	{
		//Set the Background music volume to the specified new background music volume
		backgroundMusicVolume = newBackgroundMusicVolume;
	}
}
