﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[RequireComponent(typeof(Collider))]
public class TriggerEnterSimple : Trigger
{
    //[Header("Fire on both enter and exit?")]
    //[Tooltip("With this ticked, the reaction will trigger when an object both enters and exits the trigger zone, otherwise it will only trigger when it enters")]
    //public bool enterAndExit = false;
    [Header("When the collider is entered, these will ALSO trigger")]
    public List<Reaction> reactionsToTrigger = new List<Reaction>();
    [Header("Will only trigger if the gameobject has ONE or MORE of these IDs")]
    public List<int> ids = new List<int>();



    void Start()
    {
        if (!GetComponent<Rigidbody>())
        {
            gameObject.AddComponent<Rigidbody>().isKinematic = true;
        }
        if (!GetComponent<Collider>().isTrigger)
        {
            GetComponent<Collider>().isTrigger = true;
            Debug.LogWarning(this.name + " has a TriggerTriggerCollider attached to it, but the collider attached is not set to trigger. It has been set to trigger", this);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        // adds the reaction from the base class to the list, removes it at the end
        reactionsToTrigger.Add(reaction);
        foreach (Reaction item in reactionsToTrigger)
        {
            //if no ids, fires
            if (ids.Count == 0)
            {
                item.Fire(other.gameObject);
            } else
            {
                // checks to see if theres a tag so more checking can continue
                IdTag temp = other.GetComponent<IdTag>();
                if (temp != null)
                {
                    foreach (int id in temp.ids)
                    {
                        if (ids.Contains(id))
                        {
                            item.Fire(other.gameObject);
                        }
                    }
                }
            }
        }
        // remvoes the reaction from the base class from the list, as it was added before
        reactionsToTrigger.Remove(reaction);
    }

    //void OnTriggerExit(Collider other)
    //{
    //    if (enterAndExit) Fire(other.gameObject);
    //}
}
