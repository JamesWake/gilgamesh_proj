﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(ParticleSystem))]
public class ReactionParticleEffect : Reaction {
    ParticleSystem partSys;
	void Start () {
        partSys = GetComponent<ParticleSystem>();
	}

    public override void Fire(GameObject objectToCheck = null)
    {
        base.Fire(objectToCheck);
        partSys.Play();
    }
}
