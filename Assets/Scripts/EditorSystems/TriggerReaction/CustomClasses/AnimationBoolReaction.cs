﻿using UnityEngine;
using System.Collections;

public class AnimationBoolReaction : EditorReaction
{
	//The animator to use
	public Animator animator;

    //what to set it to
    public bool Set;

	//The name of the animation to run
	public string boolName = "BoolName";

	//Called when the trigger this reaction is linked to is triggered
	public override void React()
	{
		//Run the animation
		animator.SetBool(boolName, Set);
	}
}
