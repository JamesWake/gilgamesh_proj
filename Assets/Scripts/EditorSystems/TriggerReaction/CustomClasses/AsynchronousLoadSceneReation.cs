﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class AsynchronousLoadSceneReation : EditorReaction
{
	//The name of the scene to load
	public string sceneToLoad = "Default";

	//Async operation object for loading the scene
	private AsyncOperation loadScene;

	//Called when the trigger the reaction is linked to is triggered
	public override void React()
	{
		//Start Loading the Scene
		StartCoroutine("LoadGameScene");

		//Enable the scene from activating
		loadScene.allowSceneActivation = true;
	}

	private IEnumerator LoadGameScene()
	{
		//Begin loading the Game scene
		loadScene = SceneManager.LoadSceneAsync(sceneToLoad);

		//Disable the scene from immediately activating
		loadScene.allowSceneActivation = false;

		//Return from the function
		yield return loadScene;
	}
}
