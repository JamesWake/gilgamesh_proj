﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadSceneReaction : EditorReaction
{
	//The name of the scene to load
	public string sceneName = "Default";

	//Called when the trigger this reaction is linked to is triggered
	public override void React()
	{
		//Set the scene to the next scene
		SceneManager.LoadScene(sceneName);
	}
}
