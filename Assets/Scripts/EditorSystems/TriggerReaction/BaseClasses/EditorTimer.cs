﻿using UnityEngine;
using System.Collections;

public class EditorTimer : MonoBehaviour
{
	//The length of time in seconds the timer will take to stop
	public float time = 0.0f;

	//List of reactions that will be processed when the timer starts
	public GameObject[] timerStartReactions;

	//List of reactions that will be processed when the timer stops
	public GameObject[] timerStopReactions;

	//Flag representing whether the timer stop reactions have been processed
	private bool timerStopReactionsProcessed = false;

	//Called on initialization
	void Awake()
	{
		//If there is atleast one timer start reaction
		if (timerStartReactions.Length > 0)
		{
			//For each timer start reaction
			for (int timerStartReactionIndex = 0; timerStartReactionIndex < timerStartReactions.Length; timerStartReactionIndex++)
			{
				//Get all the editor reactions on the timer start reaction GameObject
				EditorReaction[] editorReactions = timerStartReactions[timerStartReactionIndex].GetComponents<EditorReaction>();

				//For each timer start reaction on the GameObject
				for (int editorReactionScriptIndex = 0; editorReactionScriptIndex < editorReactions.Length; editorReactionScriptIndex++)
				{
					//Call the timer start reaction
					editorReactions[editorReactionScriptIndex].React();
				}
			}
		}
	}

	//Called once per frame
	void Update()
	{
		//If the timer stop reactions have not been processed
		if(!timerStopReactionsProcessed)
		{
			//Decrement the timer
			time -= 1.0f * Time.deltaTime;

			//If the timer is less then zero
			if (time < 0.0f)
			{ 
				//For each timer stop reaction
				for (int timerStopReactionIndex = 0; timerStopReactionIndex < timerStopReactions.Length; timerStopReactionIndex++)
				{
					//Get all the editor reactions on the timer stop reaction GameObject
					EditorReaction[] editorReactions = timerStopReactions[timerStopReactionIndex].GetComponents<EditorReaction>();

					//For each timer stop reaction on the GameObject
					for (int editorReactionScriptIndex = 0; editorReactionScriptIndex < editorReactions.Length; editorReactionScriptIndex++)
					{
						//Call the timer stop reaction
						editorReactions[editorReactionScriptIndex].React();
					}
				}

				//Set the timer stop reactions processed flag to true
				timerStopReactionsProcessed = true;
			}
		}
	}
}
