﻿using UnityEngine;
using System.Collections;

//Required components
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]

public class PlayerController : MonoBehaviour
{
	//Singleton instance object
	public static PlayerController Instance;

	//The players camera controller
	public CameraController cameraController;

	//The minimum velocity the player can have
	public float minVelocity = -1.0f;

	//The maximum velocity the player can have
	public float maxVelocity = 2.5f;

	//The players acceleration speed
	public float accelerationSpeed = 10.0f;

	//The players deceleration speed
	public float decelerationSpeed = 5.0f;

	//The players turning speed
	public float turnSpeed = 90.0f;

	//The force to apply to the player in the up direction to initiate a jump
	public float jumpForce = 4.0f;

	//The height of the player
	public float playerHeight = 2.0f;

	//The radius of the player
	public float playerRadius = 0.5f;

	//I'm trying to add the guts of the animator setup -James
	public Animator playerAnim;

    //True when the player is in the air -James
    public bool jumping;

    //True when the player is crouching
    private bool crouching = false;

    //True when the player is throwing bread
    private bool throwing = false;

	//The players rigidbody
	private Rigidbody playerRigidbody;

	//The players forward velocity
	private float forwardVelocity;

	//Called on initialization
	void Awake()
	{ 
		//Initialize the PlayerController
		Instance = this;
		playerRigidbody = GetComponent<Rigidbody>();
		playerRigidbody.freezeRotation = true;

        playerAnim = GetComponentInChildren<Animator>();
    }

	//Called once per frame
	void Update()
	{
		//If the "W" key is down
		if(Input.GetKey(KeyCode.W) && !crouching && !throwing)
		{
			//Add a force to the player in his forward direction
			forwardVelocity += accelerationSpeed * Time.deltaTime;
		}

		//If the "S" key is down
		if(Input.GetKey(KeyCode.S) && !crouching && !throwing)
		{
			//Add a force to the player in his backward direction
			forwardVelocity -= accelerationSpeed * Time.deltaTime;
		}

		//If the "A" key is down
		if(Input.GetKey(KeyCode.A) && !crouching && !throwing)
		{
			//Rotate the player anti-clockwise
			transform.rotation *= Quaternion.Euler(0.0f, -turnSpeed * Time.deltaTime, 0.0f);
		}

		//If the "D" key is down
		if(Input.GetKey(KeyCode.D) && !crouching && !throwing)
		{
			//Rotate the player clockwise
			transform.rotation *= Quaternion.Euler(0.0f, turnSpeed * Time.deltaTime, 0.0f);
		}

		//If the 'Space' key is down
		if(Input.GetKeyDown(KeyCode.Space) && IsGrounded())
        {
            //Gilgamesh.SetTrigger("Crouch");
            crouching = true;
        }
        //If the 'Space' key is up
        if(Input.GetKeyDown(KeyCode.Space) && IsGrounded())
        {
            crouching = false;
            //Gilgamesh.SetTrigger("Jump");
            Jump();
        }

        if (jumping && IsGrounded())
        {
            if (playerAnim) playerAnim.SetTrigger("Land");
            jumping = false;
        }

        if (Input.GetMouseButtonDown(0) && IsGrounded() && !throwing)
        {
            throwing = true;
            playerAnim.SetTrigger("Throw");
            StartCoroutine(SetThrowingFalse());
        }

		//Apply friction to the player
		ApplyFriction();

		//Move the player
		Move();

        playerAnim.SetFloat("Forward", forwardVelocity);
	}

	//Applys friction to the player
	private void ApplyFriction()
	{
		//Calculate friction force
		float frictionForce = decelerationSpeed * Time.deltaTime;

		//If the players forward velocity is positive
		if (forwardVelocity > 0.0f)
		{
			//If the players forward velocity minus the friction force is less then zero
			if (forwardVelocity - frictionForce < 0.0f)
			{
				//Clamp the players forward velocity to zero
				forwardVelocity = 0.0f;
			}
			//Otherwise
			else
			{
				//Apply the friction force to the players forward velocity
				forwardVelocity -= frictionForce;
			}
		}
		//Otherwise if the players forward velocity is negative
		else if (forwardVelocity < 0.0f)
		{
			//If the players forward velocity plus the friction force is more then zero
			if (forwardVelocity + frictionForce > 0.0f)
			{
				//Clamp the players forward velocity to zero
				forwardVelocity = 0.0f;
			}
			//Otherwise
			else
			{
				//Apply the friction force to the players forward velocity
				forwardVelocity += frictionForce;
			}
		}
	}

	//Moves the player
	private void Move()
	{
		//Clamp the players forward velocity
		forwardVelocity = Mathf.Clamp(forwardVelocity, minVelocity, maxVelocity);

        // moves the player with the rigidbody -James
        //playerRigidbody.MovePosition(transform.position + (transform.forward * forwardVelocity * Time.deltaTime));		

        //Move the player
		transform.position += transform.forward * forwardVelocity * Time.deltaTime;
	}

	//Makes the player jump
	private void Jump()
    {
        if (playerAnim) playerAnim.SetTrigger("Jump");
        //Gilgamesh.SetFloat("Forward", 0);
        //Gilgamesh.SetTrigger("Jump");
        //Apply force in the up direction to the player to initiate a jump
        playerRigidbody.AddForce(new Vector3(0.0f, jumpForce, 0.0f), ForceMode.Impulse);
        StartCoroutine(SetJumpTrue());
	}

    // WIP! setting jump true straight away made grounded and jumping true on the same frame, making it hard to check -James
    IEnumerator SetJumpTrue ()
    {
        yield return new WaitForSeconds(0.3f);
        jumping = true;
    }
    IEnumerator SetThrowingFalse ()
    {
        yield return new WaitForSeconds(0.9f);
        throwing = false;
    }

	//Returns a flag indicating whether the player is grounded
	//return false - The player is not grounded
	//return true - The player is grounded
	public bool IsGrounded()
	{
		//If the center of the bottom of the player is touching something
		if (Physics.Raycast(transform.position, -transform.up, playerHeight / 2.0f + 0.1f))
		{
			//The player is grounded
			return true;
		}

		//If the front of the bottom of the player is touching something
		if (Physics.Raycast(transform.position + new Vector3(0.0f, 0.0f, playerRadius), -transform.up, playerHeight / 2.0f + 0.1f))
		{
			//The player is grounded
			return true;
		}

		//If the back of the bottom of the player is touching something
		if (Physics.Raycast(transform.position + new Vector3(0.0f, 0.0f, -playerRadius), -transform.up, playerHeight / 2.0f + 0.1f))
		{
			//The player is grounded
			return true;
		}

		//If the left of the bottom of the player is touching something
		if (Physics.Raycast(transform.position + new Vector3(-playerRadius, 0.0f, 0.0f), -transform.up, playerHeight / 2.0f + 0.1f))
		{
			//The player is grounded
			return true;
		}

		//If the right of the bottom of the player is touching something
		if (Physics.Raycast(transform.position + new Vector3(playerRadius, 0.0f, 0.0f), -transform.up, playerHeight / 2.0f + 0.1f))
		{
			//The player is grounded
			return true;
		}

		//The player is not grounded
		return false;
	}
}
