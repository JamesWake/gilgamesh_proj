﻿using UnityEngine;
using System.Collections.Generic;

public class ReactionSplitter : Reaction {
    public List<Reaction> reactionsToFire = new List<Reaction>();

    public override void Fire(GameObject objectToCheck)
    {
        base.Fire(objectToCheck);
        if (!reactionsToFire.Contains(this))
        {
            foreach (Reaction item in reactionsToFire)
            {
                item.Fire(objectToCheck);
            }
        } else
        {
            Debug.LogWarning("YOU HAVE A REACTION SPLITTER INSIDE ITSELF, THIS WOULD CAUSE AN INFINITE LOOP BUT JAMES WAS SO CLEVER THAT HE SAW THIS COMING AND MADE IT NOT WORK.", this);
        }
    }
}
