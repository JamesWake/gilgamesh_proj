﻿using UnityEngine;
using System.Collections;

public class ReactionAddId : Reaction {
    [Header("adds the key to the specified or unspecified object")]
    [Tooltip("if you leave this blank the object will get passed on from the trigger if it can. eg a trigger collider will pass the object that triggered it")]
    public IdTag objectToAddKeyTo;
    public int keyToAdd;
    public override void Fire(GameObject objectToCheck = null)
    {
        base.Fire(objectToCheck);
        if (objectToAddKeyTo == null)
        {
            objectToAddKeyTo = objectToCheck.GetComponent<IdTag>();
        }
        if (objectToAddKeyTo != null && !objectToAddKeyTo.ids.Contains(keyToAdd))
        {
            objectToAddKeyTo.ids.Add(keyToAdd);
        }
    }
}
