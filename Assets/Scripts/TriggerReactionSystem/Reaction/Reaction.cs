﻿using UnityEngine;
using System.Collections;
/// <summary>
/// James
/// The base class to all reactions, all reactions should inherit from this
/// </summary>
public abstract class Reaction : TriggerReactionParent {
    protected bool toggle;
    /// <summary>
    /// Fire called by reactions and (hopefully) only reactions
    /// all checking should be done on triggers
    /// </summary>
    public virtual void Fire(GameObject objectToCheck = null)
    {
        toggle = !toggle;
    }
}
