﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomlyPlaySound : MonoBehaviour {
    public List<AudioClip> sounds;
    public float waitBetweenThis = 1, andThis = 5;

    void Awake ()
    {
        StartCoroutine(PlayAndWait());
    }

    IEnumerator PlayAndWait ()
    {
        while (true)
        {
            AudioManager.PlayAudio(sounds[Random.Range(0, sounds.Count-1)],gameObject, AudioPlayerType.Effect, AudioPlayingMode.Once );
            yield return new WaitForSeconds(Random.Range(waitBetweenThis, andThis));
        }
    }

}
