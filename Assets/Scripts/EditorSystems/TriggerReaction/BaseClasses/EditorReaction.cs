﻿using UnityEngine;
using System.Collections;

public class EditorReaction : MonoBehaviour
{
	//Called when the trigger the reaction is linked to is triggered
	public virtual void React() { }
}
