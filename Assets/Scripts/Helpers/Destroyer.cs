﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour
{
	//Destroys the specified GameObject
	public static void DestroyGameObject(GameObject arg)
	{
		//Destroy the specified GameObject
		Destroy(arg);
	}
}
