﻿using UnityEngine;
using System.Collections;

public class EditorTrigger : MonoBehaviour
{
	//Whether the trigger will only be triggered when all interaction GameObjects are interacting with the trigger
	public bool requireAllInteractions = false;

	//The color the trigger will be rendered
	public Color triggerColor = new Color(1.0f, 0.0f, 0.0f, 0.5f);

	//List of GameObjects that can interact with the trigger
	public GameObject[] interactionGameObjects;

	//List of reactions that have will be called when the trigger is interacted with (trigger enter reactions)
	public GameObject[] triggerEnterReactions;

	//List of reactions that have will be called when the trigger is interacted with (trigger exit reactions)
	public GameObject[] triggerExitReactions;

	//Flag representing whether the trigger is currently being interacted with
	private bool isInteracting = false;

	//Called when drawing script gizmo's
	void OnDrawGizmos()
	{
		//Sets the draw color for drawing gizmo's
		Gizmos.color = triggerColor;

		//Draw the trigger
		Gizmos.DrawCube(transform.position, transform.lossyScale);
	}

	//Called once per frame
	void Update()
	{
		//The number of GameObject interacting with the trigger
		int numInteractingGameObjects = 0;

		//For each GameObject in the interaction GameObjects list
		for(int gameObjectIndex = 0; gameObjectIndex < interactionGameObjects.Length; gameObjectIndex++)
		{
			//If the interaction GameObject is valid
			if (interactionGameObjects[gameObjectIndex] != null)
			{
				//If the GameObject is interacting with the trigger
				if (AABB(transform.position, transform.lossyScale, interactionGameObjects[gameObjectIndex].transform.position, interactionGameObjects[gameObjectIndex].transform.lossyScale))
				{
					//Increment the number of interacting GameObjects
					numInteractingGameObjects++;

					//If the trigger does not require all interacting GameObjects to interact with the trigger
					if (!requireAllInteractions)
					{
						//Break out of the loop (There is no point in checking for more collisions)
						break;
					}
				}
			}
		}

		//If the trigger required interacting with all interaction GameObjects to trigger
		if(requireAllInteractions)
		{
			//If all the interaction GameObjects are not interacting with the trigger
			if(numInteractingGameObjects != interactionGameObjects.Length)
			{
				//Return from the function
				return;
			}
		}

		//If the number of GameObjects interacting with the trigger is more then zero
		if(numInteractingGameObjects > 0)
		{
			//If the is interacting flag is not currently set
			if(!isInteracting)
			{
				//For each enter trigger reaction
				for(int enterReactionIndex = 0; enterReactionIndex < triggerEnterReactions.Length; enterReactionIndex++)
				{
					//Get all the editor reactions on the enter reaction GameObject
					EditorReaction[] editorReactions = triggerEnterReactions[enterReactionIndex].GetComponents<EditorReaction>();

					//For each editor reaction on the GameObject
					for(int editorReactionScriptIndex = 0; editorReactionScriptIndex < editorReactions.Length; editorReactionScriptIndex++)
					{
						//Call the trigger enter reaction
						editorReactions[editorReactionScriptIndex].React();
					}
				}

				//Set the is interacting flag to true
				isInteracting = true;
			}
		}
		//Otherwise
		else
		{
			//If the is interacting flag is currently set
			if(isInteracting)
			{
				//For each exit trigger reaction
				for (int exitReactionIndex = 0; exitReactionIndex < triggerExitReactions.Length; exitReactionIndex++)
				{
					//Get all the editor reactions on the exit reaction GameObject
					EditorReaction[] editorReactions = triggerExitReactions[exitReactionIndex].GetComponents<EditorReaction>();

					//For each editor reaction on the GameObject
					for (int editorReactionScriptIndex = 0; editorReactionScriptIndex < editorReactions.Length; editorReactionScriptIndex++)
					{
						//Call the trigger enter reaction
						editorReactions[editorReactionScriptIndex].React();
					}
				}

				//Set the is interacting flag to false
				isInteracting = false;
			}
		}
	}

	//Returns true if the points intersect collide
	public bool AABB(Vector3 position1, Vector3 scale1, Vector3 position2, Vector3 scale2)
	{
		//Modify the scales
		scale1 /= 2.0f;
		scale2 /= 2.0f;

		//Return whether the points intersect
		return (position1.x - scale1.x <= position2.x + scale2.x && position1.x + scale1.x >= position2.x - scale2.x) &&
			   (position1.y - scale1.y <= position2.y + scale2.y && position1.y + scale1.y >= position2.y - scale2.y) &&
			   (position1.z - scale1.z <= position2.z + scale2.z && position1.z + scale1.z >= position2.z - scale2.z);
	}
}
