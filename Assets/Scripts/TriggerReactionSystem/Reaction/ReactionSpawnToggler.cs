﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Spawns prefabs on a delay over and over again
/// -James
/// </summary>
public class ReactionSpawnToggler : Reaction {
    
    public float timeBetweenSpawn;
    public GameObject prefab;
    public bool repeat = false;
    void Awake ()
    {
        if (repeat) StartCoroutine(SpawnPrefabOnTimer());
    }
    /// <summary>
    /// spawns object at its own postition, waits timeBetweenSpawn, starts over
    /// </summary>
    /// <returns></returns>
    IEnumerator SpawnPrefabOnTimer ()
    {
        while (true)
        {
            if (toggle)
            {
                Instantiate(prefab, transform.position, Quaternion.identity);
            }
            yield return new WaitForSeconds(timeBetweenSpawn);
        }
    }

    public override void Fire(GameObject objectToCheck)
    {
        base.Fire(objectToCheck);
        if (!repeat) Instantiate(prefab, transform.position, Quaternion.identity);
    }
}