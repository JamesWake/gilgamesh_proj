﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class AnimatorEventReciever : MonoBehaviour {
    public AudioSource footAudioSource;

    public ParticleSystem footParticleSystem;

    public List<AudioClip> footsteps;

    public List<AudioClip> jumpSounds;

    public List<AudioClip> landSounds;

    public List<AudioClip> throwSounds;

    void PlayFootstep()
    {
        if (footAudioSource)
        {
            AudioManager.PlayAudio(footsteps[Random.Range(0, footsteps.Count)], gameObject , AudioPlayerType.Effect, AudioPlayingMode.Once);
        }

        if (footParticleSystem)
        {
            footParticleSystem.Play();
        }
    }

    void JumpEffect ()
    {
        if (footAudioSource)
        {
            AudioManager.PlayAudio(jumpSounds[Random.Range(0, jumpSounds.Count - 1)], gameObject, AudioPlayerType.Effect, AudioPlayingMode.Once);

        }
        if (footParticleSystem)
        {
            footParticleSystem.Play();
        }
    }

    void ThrowEffect()
    {
        AudioManager.PlayAudio(throwSounds[Random.Range(0, throwSounds.Count)], AudioPlayerType.Effect, AudioPlayingMode.Once);
    }

    void LandEffect ()
    {
        if (footAudioSource)
        {
            AudioManager.PlayAudio(landSounds[Random.Range(0, landSounds.Count-1)], gameObject, AudioPlayerType.Effect, AudioPlayingMode.Once);
        }
        if (footParticleSystem)
        {
            footParticleSystem.Play();
        }
    }


}
