﻿using UnityEngine;
using System.Collections;

public static class AudioPlayerType
{
    //Numberic representation of a Standard AudioPlayer
    public const ushort Standard = 0;

    //Numberic representation of an Effect AudioPlayer
    public const ushort Effect = 1;

    //Numberic representation of a BackgroundMusic AudioPlayer
    public const ushort BackgroundMusic = 2;
}
