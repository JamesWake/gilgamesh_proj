﻿using UnityEngine;
using System.Collections;

public class AimController : MonoBehaviour
{
	//The players bread manager
	public BreadManager breadManager;

	//The aim GameObject
	public GameObject aim;

	//The sprite to use when the player is aiming at a valid location
	public Sprite aimTextureValid;

	//The sprite to use when the player is aiming at an invalid location
	public Sprite aimTextureInvalid;

	//The radius the player can throw/cast bread
	public float aimRadius = 10.0f;

	//The deadzone the play cannot throw/cast bread within
	public float aimDeadzone = 1.5f;

	//The amount to offset the aim texture on the y-axsi
	public float offsetY = 0.1f;

	//Called once per frame
	void Update()
	{
		//Create ray for raycast
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		//Create array of raycast hits
		RaycastHit[] hitList;

		//Get all the objects between the cameras position and the mouses position in 3D space
		hitList = Physics.RaycastAll(ray);

		//If atleast one GameObject was hit
		if (hitList.Length > 0)
		{
			//Closest hit distance
			float closestHitDistance = Mathf.Infinity;

			//Closest hit index
			int closestHitIndex = 0;

			//For each GameObject hit
			for (int hitIndex = 0; hitIndex < hitList.Length; hitIndex++)
			{
				//If the GameObject hit does not have a DisableRaycast component
				if (hitList[hitIndex].transform.GetComponent<DisableRaycast>() == null)
				{
					//Calculate the distance the hit GameObject is from the camera
					float distanceFromCamera = Vector3.Distance(hitList[hitIndex].point, Camera.main.transform.position);

					//If the GameObject hit is closer then that previously closest recorded hit
					if (distanceFromCamera < closestHitDistance)
					{
						//Set the closest hit distance to the distance from camera
						closestHitDistance = distanceFromCamera;

						//Set the closest hit index to the current hit index
						closestHitIndex = hitIndex;
					}
				}
			}

			//Move the aim GameObject to the closest hit GameObjects position plus the offset
			aim.transform.position = hitList[closestHitIndex].point + new Vector3(0.0f, offsetY, 0.0f);

			//Rotate the aim GameObject to reflect the closest hit GameObjects rotation
			aim.transform.rotation = Quaternion.FromToRotation(transform.up, hitList[closestHitIndex].normal) * transform.rotation * Quaternion.Euler(new Vector3(90.0f, 0.0f, 0.0f));
		}

		//Calculate the distance from the player to the aim GameObject
		float aimDistance = Vector3.Distance(transform.position, aim.transform.position);

		//If the players desired aim position is within the players aim radius
		if (aimDistance <= aimRadius && aimDistance > aimDeadzone)
		{
			//Set the aim texture to the valid aiming texture
			aim.GetComponent<SpriteRenderer>().sprite = aimTextureValid;

			//If the player pressed the left mouse button down this frame
			if (Input.GetMouseButtonDown(0))
			{
				//Spawn a piece of bread
				breadManager.SpawnBread(aim.transform.position, aim.transform.rotation);
			}
		}
		else
		{
			//Set the aim texture to the invalid aiming texture
			aim.GetComponent<SpriteRenderer>().sprite = aimTextureInvalid;
		}
	}
}
