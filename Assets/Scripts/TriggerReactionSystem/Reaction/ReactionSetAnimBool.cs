﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Animator))]
public class ReactionSetAnimBool : Reaction
{
    public string nameOfBool;
    Animator anim;
    void Start ()
    {
        anim = GetComponent<Animator>();
    }

    override public void Fire (GameObject objectToCheck = null)
    {
        base.Fire(objectToCheck);
        if (anim != null)
            anim.SetBool(nameOfBool, !anim.GetBool(nameOfBool));
        else
        {
            Debug.LogWarning("No animator found on this object", this);
        }
    }
    
}
