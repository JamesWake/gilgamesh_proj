﻿using UnityEngine;
using System.Collections;

public class ReactionMoveObject : Reaction {
    [Tooltip("the object in this field will be moved")]
    public GameObject objectToMove;
    [Tooltip("the object in 'object to move' will move towards this object at the above speeds")]
    public GameObject endPosition;

    [Header("Movement")]
    //[SerializeField]
    [Tooltip("the distance object to move will travel every second - 0 will make it instant")]
    public float moveSpeed;
    [Header("Dont move on this axis?")]
    public bool xAxis = true;
    public bool yAxis = true;
    public bool zAxis = true;


    [Header("Rotation")]
    [Tooltip("'Object to move' will only rotate if this box is ticked")]
    public bool alsoRotateToMatch = false;
    [Tooltip("the ammount object to move will rotate every second in degrees - 0 will make it instant")]
    public float rotateSpeed;

    public override void Fire(GameObject objectToCheck = null)
    {
        Vector3 endPos = endPosition.transform.position;

        if (!xAxis) endPos.x = objectToMove.transform.position.x;
        if (!yAxis) endPos.y = objectToMove.transform.position.y;
        if (!zAxis) endPos.z = objectToMove.transform.position.z;

        base.Fire(objectToCheck);
        // if the move speed is 0, moves it straight away, otherwise moves it over time in the blow coroutine
        if (moveSpeed != 0){
            StartCoroutine(MoveOverTime(endPos));
        } else {
            objectToMove.transform.position = endPos;
        }
        // movement and rotation is split up because they dont always line up
        if (alsoRotateToMatch)
        {
            if (rotateSpeed != 0)
            {
                StartCoroutine(RotateOverTime());
            } else
            {
                objectToMove.transform.localRotation = endPosition.transform.localRotation;
            }
        }
    }

    IEnumerator MoveOverTime(Vector3 endPos)
    {
        while (Vector3.Distance(objectToMove.transform.position, endPosition.transform.position) > 0.01f )
        {
            objectToMove.transform.position = Vector3.MoveTowards(objectToMove.transform.position, endPosition.transform.position, moveSpeed * Time.deltaTime);
            yield return null;
        }
        objectToMove.transform.position = endPosition.transform.position;
    }

    IEnumerator RotateOverTime ()
    {
        //the dot product moves towards 1 the closer 2 angles are to each other
        while (Quaternion.Dot(objectToMove.transform.localRotation, endPosition.transform.localRotation) < 0.999f)
        {
            objectToMove.transform.localRotation = Quaternion.RotateTowards(objectToMove.transform.localRotation, endPosition.transform.localRotation, rotateSpeed * Time.deltaTime);
            yield return null;
        }
        objectToMove.transform.localRotation = endPosition.transform.localRotation;
    }
}
