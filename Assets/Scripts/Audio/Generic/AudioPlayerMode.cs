﻿using UnityEngine;
using System.Collections;

public static class AudioPlayerMode
{
    //Numberic representation of the Standby mode for AudioPlayers (When an audio player is not playing any sound)
    public const ushort Standby = 0;

    //Numberic representation of the Playing mode for AudioPlayers (When an audio player is playing sound)
    public const ushort Playing = 1;

    //Numberic representation of the Invalid mode for AudioPlayers (When an audio player does not have a valid AudioSource)
    public const ushort Invalid = 2;
}
