﻿using UnityEngine;
using System.Collections;

public class PlayAudioReaction : EditorReaction
{
	//The audio clip to play
	public AudioClip audioClip;

	//Whether the audio clip should loop
	public bool looping = false;

	//Whether the audio clip is music
	public bool music = false;

	//Whether the audio clip should be played in 3D/2D mode
	public bool is3D = false;

	//Called when the trigger this reaction is linked to is triggered
	public override void React()
	{
		//Create default flags
		ushort playerType = AudioPlayerType.Effect;
		ushort playingMode = AudioPlayingMode.Once;

		//If the audio clip is supposed to be music
		if (music)
		{
			//Set the audio clips player type to background music
			playerType = AudioPlayerType.BackgroundMusic;
		}

		//If the audio clip is supposed to be looping
		if (looping)
		{
			//Set the audio clips playing mode to looping
			playingMode = AudioPlayingMode.Looping;
		}

		//If the audio clip is supposed to be played in 3D
		if (is3D)
		{
			//Play the audio clip in 3D
			AudioManager.PlayAudio(audioClip, gameObject, playerType, playingMode);
		}
		//Otherwise
		else
		{
			//Play the audio clip in 2D
			AudioManager.PlayAudio(audioClip, playerType, playingMode);
		}
	}
}
