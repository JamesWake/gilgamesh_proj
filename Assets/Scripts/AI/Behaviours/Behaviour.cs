﻿using UnityEngine;
using System.Collections;

public class Behaviour
{
    //The GameObject that owns this behaviour
    protected GameObject owner;

    //Default Behave function
    public virtual void Behave() {}

    //Sets the owner GameObject of this behaviour
    protected void SetOwner(GameObject arg)
    {
        //Set the owner GameObject to the specified GameObject
        owner = arg;
    }
}
