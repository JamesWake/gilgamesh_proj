﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ReactionCheckingGate : Reaction {
    [SerializeField]
    [Tooltip("This reaction will be fired when all conditions are met")]
    protected Reaction reaction;

    [Header("Things To Check Against")]
    [SerializeField]
    [Tooltip("Will Check if object that triggered this has these tags.")]
    List<string> tagsToCheck = new List<string>();
    [SerializeField]
    [Tooltip("Will Check if object that triggered this is on these layers.")]
    List<int> layersToCheck = new List<int>();
    [SerializeField]
    [Tooltip("Will Check if object that triggered this is these specific Game Objects.")]
    List<GameObject> gameObjectsToCheck = new List<GameObject>();
    [SerializeField]
    [Tooltip("Will check if object that triggered this has these specific IDs.")]
    List<int> idsToCheck = new List<int>();
    [SerializeField]
    [Tooltip("Will only fire if all conditions listed above are met, instad of just one")]
    bool mustHaveAllToBeTriggered = false;

    public override void Fire(GameObject objectToCheck)
    {
        if (objectToCheck == null)
        {
            Debug.LogWarning("You are trying to check nothing, this will automatically trigger its reaction.", this);
            reaction.Fire(objectToCheck);
        }
        else
        {
            base.Fire(objectToCheck);
            if (CheckIfCanFire(objectToCheck))
            {
                reaction.Fire(objectToCheck);
            }
        }
    }

    /// <summary>
    /// Put all checks here, they will be run down in order and return true if its fine to Fire
    /// </summary>
    /// <returns>returns false if any check fails otherwise true</returns>
    virtual protected bool CheckIfCanFire(GameObject checkThis)
    {
        bool noFalseFound = true;
        if (tagsToCheck.Count == 0 && layersToCheck.Count == 0 && gameObjectsToCheck.Count == 0 && idsToCheck.Count == 0)
        {
            Debug.LogWarning("Nothing to check in " + gameObject.name + "'s list of conditions to check. So it wont fire. -James");
            return false;
        }

        foreach (string tag in tagsToCheck)
        {
            if (checkThis.tag == tag && !mustHaveAllToBeTriggered)
            {
                return true;
            }
            else if (checkThis.tag != tag && mustHaveAllToBeTriggered)
            {
                noFalseFound = false;
            }
        }

        foreach (int layer in layersToCheck)
        {
            if (checkThis.layer == layer && !mustHaveAllToBeTriggered)
            {
                return true;
            }
            else if (checkThis.layer != layer && mustHaveAllToBeTriggered)
            {
                noFalseFound = false;
            }
        }

        foreach (GameObject gO in gameObjectsToCheck)
        {
            if (checkThis == gO && !mustHaveAllToBeTriggered)
            {
                return true;
            }
            else if (checkThis != gO && mustHaveAllToBeTriggered)
            {
                noFalseFound = false;
            }
        }

        IdTag temp = checkThis.GetComponentInChildren<IdTag>();
        int counter = 0;
        if (temp != null)
        {
            foreach (int iD in idsToCheck)
            {
                foreach (int goIds in temp.ids)
                {
                    if (iD == goIds)
                    {
                        if (!mustHaveAllToBeTriggered)
                        {
                            return true;
                        }
                        counter++;
                    }
                }
            }
            if (mustHaveAllToBeTriggered && counter != idsToCheck.Count)
            {
                noFalseFound = false;
            }
            else if (!mustHaveAllToBeTriggered && counter == idsToCheck.Count && idsToCheck.Count != 0)
            {
                return true;
            }
        }
        else
        {
            noFalseFound = false;
        }

        if (noFalseFound && mustHaveAllToBeTriggered)
        {
            return true;
        }

        return false;
    }
}
