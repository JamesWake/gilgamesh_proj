﻿using UnityEngine;
using System.Collections;

//Required components
[RequireComponent(typeof(NavMeshAgent))]

public class DuckController : MonoBehaviour
{
	//The ducks acceleration speed
	public float accelerationSpeed = 5.0f;

	//The ducks deceleration speed
	public float decelerationSpeed = 2.5f;

	//The ducks minimum velocity (Maximum backwards speed)
	public float minVelocity = 0.0f;

	//The ducks maximum velocity (Maximum forward speed)
	public float maxVelocity = 2.0f;

	//The speed in which the duck can rotate
	public float lookSpeed = 5.0f;

	//The distance the duck will follow the player at
	public float playerFollowDistance = 3.0f;

	//The distance the duck will follow other ducks at
	public float duckFollowDistance = 2.0f;

	//The distance the duck will follow bread at
	public float breadFollowDistance = 1.0f;

	//The distance the duck will follow generic objects
	public float genericFollowDistance = 1.0f;

	//The time it takes for a duck to eat bread
	public float breadEatTime = 5.0f;

	//The distance a duck can look in any given direction
	public float duckLookDistance = 10.0f;

	//List of quack sound effects
	public AudioClip[] quackAudioList;

	//The ducks behaviour
	private Behaviour duckBehaviour;

	//Flag representing whether the duck is busy
	private bool busy = false;

	//The ducks forward velocity
	private float forwardVelocity = 0.0f;

	//The ducks animator
	private Animator duckAnimator;

	//Called on initialization
	void Awake()
	{
		//Initialize DuckController
		duckBehaviour = new StayBehaviour(gameObject, transform.position, genericFollowDistance);
		duckAnimator = GetComponent<Animator>();
	}

	//Called once per frame
	void Update()
	{
		//Perform the ducks behaviour
		duckBehaviour.Behave();

		//Apply friction to the duck
		ApplyFriction();

		//Move the duck
		Move();

		//Animate the duck
		Animate();
	}

	//Returns whether the duck is busy
	public bool IsBusy()
	{
		//Return the ducks busy flag
		return busy;
	}

    //Sets the ducks behaviour to the specified behaviour
    public void SetBehaviour(Behaviour behaviour)
    {
        //Set the ducks behaviour to the specified behaviour
        duckBehaviour = behaviour;
    }

	//Sets whether the duck is busy
	public void SetBusy(bool flag)
	{
		//Set the ducks busy flag to the specified flag
		busy = flag;
	}

	//Rotates the GameObject with this script attached using it's look speed to look at the specified location
	public void LookAtLocation(Vector3 location)
	{
		//Temporary Quaternion storing the current rotation information
		Quaternion currentRotation = transform.rotation;

		//Rotate the GameObject to look at the target GameObject
		transform.LookAt(location);

		//Smoothly look at the specified location
		transform.rotation = Quaternion.Slerp(currentRotation, transform.rotation, lookSpeed * Time.deltaTime);
	}

	//Adds a force to the GameObject with this script attached in it's forward direction
	public void AddForwardForce()
	{
		//Add a force to the duck accelerating the speed it is moving in it's forward direction
		forwardVelocity += accelerationSpeed * Time.deltaTime;
	}

	//Makes the duck quack
	public void Quack()
	{
		//Play the duck quack animation
		AudioManager.PlayAudio(quackAudioList[Random.Range(0, quackAudioList.Length)], gameObject, AudioPlayerType.Effect, AudioPlayingMode.Once);
	}

	//Returns the GameObjects modified forward direction with a clamped y value of 0
	private Vector3 GetModifiedForward()
	{
		//Return the owner GameObjects forward direction with a clamped y value of 0
		return new Vector3(transform.forward.x, 0.0f, transform.forward.z);
	}

	//Applys friction to the duck
	private void ApplyFriction()
	{
		//Calculate friction force
		float frictionForce = decelerationSpeed * Time.deltaTime;

		//If the ducks positive forward velocity minus the friction force is less then zero
		if(Mathf.Abs(forwardVelocity) - frictionForce < 0.0f)
		{
			//Clamp the ducks forward velocity to zero
			forwardVelocity = 0.0f;
		}
		//Otherwise
		else
		{
			//If the ducks forward velocity is positive
			if(forwardVelocity > 0.0f)
			{
				//Apply friction to duck
				forwardVelocity -= frictionForce;
			}
			//Otherwise
			else
			{
				//Apply friction to duck
				forwardVelocity += frictionForce;
			}
		}
	}

	//Moves the duck
	private void Move()
	{
		//Clamp the ducks forward velocity
		forwardVelocity = Mathf.Clamp(forwardVelocity, minVelocity, maxVelocity);

		//Move the duck
		transform.position += transform.forward * forwardVelocity * Time.deltaTime;
	}

	//Animates the duck
	private void Animate()
	{
		//If the ducks forward velocity is more then zero
		if (forwardVelocity > 0.0f)
		{
			//Enable the ducks walking animation
			duckAnimator.SetBool("Walk", true);
		}
		//Otherwise
		else
		{
			//Disable the ducks walking animation
			duckAnimator.SetBool("Walk", false);

			//If the duck is eating
			if (IsBusy())
			{
				//Enable the ducks eating animation
				duckAnimator.SetBool("Eat", true);
			}
			//Otherwise
			else
			{
				//Disable the ducks eating animation
				duckAnimator.SetBool("Eat", false);
			}
		}
	}
}
