﻿using UnityEngine;
using System.Collections;

public class TeleportReaction : EditorReaction
{
	//The GameObject to teleport
	public GameObject teleportGameObject;

	//Called when the trigger this reaction is linked to is triggered
	public override void React()
	{
		//Teleport the GameObject to the specified position and rotation
		teleportGameObject.transform.position = transform.position;
		teleportGameObject.transform.rotation = transform.rotation;
	}
}
