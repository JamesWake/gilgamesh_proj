﻿using UnityEngine;
using System.Collections;

public class EatBehaviour : Behaviour
{
	//The GameObject to follow
	private GameObject target;

	//The Distance away from the target GameObject where the GameObject with this script attached can eat the target GameObject
	private float eatDistance;

	//The time required for the GameObject with this script attached to finnish eating the target GameObject
	private float eatTime;

	//The AI's NavMeshAgent
	private NavMeshAgent navMeshAgent;

	//The AI's duck controller
	private DuckController duckController;

	//Creates and initializes a EatBehaviour instance
	public EatBehaviour(GameObject owner = null, GameObject target = null, float eatDistance = 1.0f, float eatTime = 5.0f)
	{
		//Initialize EatBehaviour
		SetOwner(owner);
		SetTarget(target);
		SetEatDistance(eatDistance);
		SetEatTime(eatTime);
		owner.GetComponent<DuckController>().SetBusy(true);
		navMeshAgent = owner.GetComponent<NavMeshAgent>();
		navMeshAgent.Stop();
		duckController = owner.GetComponent<DuckController>();
	}

	//Performs the operations necessary for the follow behaviour
	public override void Behave()
	{
		//If the owner GameObject exists
		if (owner != null)
		{
			//Disable automatic AI movement from the AI's nav mesh agent
			navMeshAgent.Stop();

			//If the target GameObject exists
			if (target != null)
			{
				//If the GameObject with this script attached is close enough to the target GameObject to eat it
				if(Vector3.Distance(owner.transform.position, target.transform.position) <= eatDistance)
				{
                    //Decrement the eat time
                    eatTime -= 1.0f * Time.deltaTime;

					//If the eat time is less or equal to zero
					if(eatTime <= 0.0f)
					{
                        //Destroy the target GameObject
                        Destroyer.DestroyGameObject(target);
						target = null;
					}
				}
				//Otherwise
				else
				{
					//Set the AI's destination to the AI's stay location
					navMeshAgent.SetDestination(target.transform.position);

					//If the AI is not already at it's destination
					if (navMeshAgent.path.corners.Length > 1)
					{
						//Rotate to look at the next corner in the AI's path
						duckController.LookAtLocation(navMeshAgent.path.corners[1]);

						//Move the AI Forward
						duckController.AddForwardForce();
                    }
				}
			}
			//Otherwise
			else
			{
				//Set the GameObject with this script attached as not busy
				owner.GetComponent<DuckController>().SetBusy(false);
			}
		}
	}

	//Sets the target GameObject (The object to follow)
	public void SetTarget(GameObject arg)
	{
		//Set the target GameObject to the specified GameObject
		target = arg;
	}

	//Sets the eat distance (The distance away from the target GameObject where the GameObject with this script attached can eat the target GameObject)
	public void SetEatDistance(float arg)
	{
		//Set the eat distance to the specified argument
		eatDistance = arg;
	}

	//Sets the eat time (The amount of time required to eat the target GameObject
	public void SetEatTime(float arg)
	{
		//Set the eat time to the specified argument
		eatTime = arg;
	}
}
