﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// used in the first puzzle, be ready to drop this script if it gets buggy
/// -James
/// </summary>

public class Car : SuperBehaviour {
    [SerializeField]
    float speed;
    [Header("The car moves from start to the end, the jumps back to the start")]
    public GameObject start;
    public GameObject end;

    [Header("Cars will stop for this object and other cars")]
    public GameObject stopForThisObject;

    public AudioClip StopSound;

    bool waitingOnTeleport = false;

    bool stopPlayed = true;
    bool stopped = false;

    Animator anim;
    Rigidbody rb;

    /// <summary>
    /// this is here to keep from declaring it every frame
    /// </summary>
    RaycastHit[] hits;
    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponentInChildren<Animator>();
    }
    void Update()
    {
        //Car temp = null;
        hits = Physics.RaycastAll(gameObject.transform.position + (up * 0.5f), transform.forward, 2f);
        bool stop = false;
        foreach (RaycastHit item in hits)
        {
            if(item.transform.gameObject == stopForThisObject)
            {
                if(!stopPlayed)
                {
                    AudioManager.PlayAudio(StopSound, AudioPlayerType.Effect, AudioPlayingMode.Once);
                }
            }

            if (item.transform.gameObject.GetComponent<Car>() != null || item.transform.gameObject == stopForThisObject)
            {
                stop = true;
            }
            if (item.transform.gameObject == stopForThisObject && stopped == true)
            {
                stopped = true;
            }
        }
        if (!stop)
        {
            MoveTowardsEnd();
            if (stopPlayed)
                stopPlayed = false;
        }
        else
        {
            if (!stopPlayed)
            {
                anim.SetTrigger("Stop");
                stopPlayed = true;
            }
        }   
    }

    void MoveTowardsEnd()
    {
        rb.MovePosition(Vector3.MoveTowards(transform.position, end.transform.position, speed * Time.fixedDeltaTime));
        transform.LookAt(end.transform.position);
        if (!waitingOnTeleport && Vector3.Distance(transform.position, end.transform.position) < 0.5f)
        {
            waitingOnTeleport = true;
            StartCoroutine(ReturnToStart());
        }
    }

    IEnumerator ReturnToStart ()
    {
        yield return new WaitForSeconds(Random.Range(1, 1));
        transform.position = start.transform.position;
        waitingOnTeleport = false;
    }
}
