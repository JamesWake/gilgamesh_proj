﻿using UnityEngine;
using System.Collections;

public class ReactionTimer : Reaction {
    [Header("When triggered, waits x time, then triggers next reaction")]
    public Reaction reaction;
    public float timeUntilTrigger;
    // Use this for initialization

    public override void Fire(GameObject objectToCheck = null)
    {
        base.Fire(objectToCheck);
        StartCoroutine(TriggerInSeconds());
    }

    IEnumerator TriggerInSeconds()
    {
        yield return new WaitForSeconds(timeUntilTrigger);
        reaction.Fire();
    }

}
