﻿using UnityEngine;
using System.Collections;

public class AnimationTriggerReaction : EditorReaction
{
	//The animator to use
	public Animator animator;

	//The name of the animation to run
	public string triggerName = "TriggerName";

	//Called when the trigger this reaction is linked to is triggered
	public override void React()
	{
		//Run the animation
		animator.SetTrigger(triggerName);
	}
}
