﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public static CameraController instance;

	//The object the camera will follow
	public GameObject target;

	//The position offset for the camera from the target GameObject
	public Vector3 positionOffset;

	//The rotation offset for the camera from the target GameObject
	public Vector3 rotationOffset;

    // the speed at which the camera moves towards it target
    public float camLookSpeed=3;

	//The speed the camera smoothly follows the target GameObjects position
	public float positionDamping = 2.0f;

	//The speed the camera smoothly follows the target GameObjects rotation
	public float rotationDamping = 180f;

    // the magnitude at which the camera tries to predict the players movement
    public float camLookPrediction = 2;

    public int ignoreThisPhysicsLayer;

    Camera cam;

    Vector3 camDefaultPos = new Vector3 (0,3f,-5f);

    //public GameObject temp;

    // WIP adds an offset so the camera is looking where the player wants to go -James
    Vector3 lookAtPos;

    public bool doCameraCollision = true;

    void Awake ()
    {
        instance = this;
        cam = GetComponentInChildren<Camera>();
    }

    void Start ()
    {
        lookAtPos = target.transform.position + (target.transform.forward * 2) + (InputOffset() * camLookPrediction);
    }

	//Called once per frame
	void Update()
	{
        // makes the cam move behind the player
        transform.rotation = Quaternion.RotateTowards(transform.rotation, target.transform.rotation, Quaternion.Angle(transform.rotation, target.transform.rotation) * rotationDamping * Time.deltaTime);

        //Smoothly move the camera to it's desired position
        transform.position = Vector3.MoveTowards(transform.position, target.transform.position, Time.deltaTime * positionDamping);

        // adds the look camLookPrediction
        Vector3 offset = target.transform.position + (target.transform.forward * 3) + (InputOffset() * camLookPrediction);

        // this line of code sorts out where the look at pos should be -James
        lookAtPos = Vector3.MoveTowards(
            // where it is -James
            lookAtPos,
            // its target             +     ahead of the player        +  an offset that changes as the player presses movement keys -James
            offset,
            // the value it moves by is clamped between deltatime and deltatime*8, it also gets higher as the distance gets greater so the camera doesnt get too lost -James
            Vector3.Distance(lookAtPos, offset) * camLookSpeed * Time.deltaTime);
        
        // the final look
        cam.transform.LookAt(lookAtPos);

        if (doCameraCollision)
        {
            // camera col

            // figures out where the cam should be
            Vector3 rayOffsetDesLoc = (target.transform.forward * -5) + (target.transform.up * 3);

            rayOffsetDesLoc += target.transform.position;

            RaycastHit hit;

            // if hit, move, if not, go to default pos

            if (Physics.Linecast(transform.position, rayOffsetDesLoc, out hit, ignoreThisPhysicsLayer, QueryTriggerInteraction.Ignore))
            {
                cam.transform.position = hit.point;
            }
            else
            {
                cam.transform.localPosition = camDefaultPos;
            }
        }
    }

    // pretty self explanatory, press a key, modify the vector -James
    public Vector3 InputOffset()
    {
        Vector3 value = new Vector3();

        if (Input.GetKey(KeyCode.W))
        {
            value += target.transform.forward.normalized;
        }
        
        else if (Input.GetKey(KeyCode.S))
        {
            value += -target.transform.forward.normalized;
        }
        
        if (Input.GetKey(KeyCode.A) )
        {
            value += -target.transform.right.normalized;
        }
        
        else if (Input.GetKey(KeyCode.D))
        {
            value += target.transform.right.normalized;
        }

        return value;
        
    }

	//Returns the camera's forward direction with a clamped y value of 0
	public Vector3 GetModifiedForward()
	{
		//Return the camera's forward direction with a clamped y value of 0
		return new Vector3(transform.forward.x, 0.0f, transform.forward.z);
	}

	//Returns the camera's right direction with a clamped y value of 0
	public Vector3 GetModifiedRight()
	{
		//Return the camera's right direction with a clamped y value of 0
		return new Vector3(transform.right.x, 0.0f, transform.right.z);
	}
}
