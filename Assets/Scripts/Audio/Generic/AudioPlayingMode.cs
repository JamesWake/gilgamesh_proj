﻿using UnityEngine;
using System.Collections;

public static class AudioPlayingMode
{
    //Numberic representation of the play once audio playing mode
    public const ushort Once = 0;

    //Numberic representation of the play looping audio playing mode
    public const ushort Looping = 1;
}
