﻿using UnityEngine;
using System.Collections;

/// <summary>
/// James
/// Basically so i can set all triggers and reactions to work to my spec, ie [DisallowMultipleComponent] enforces a better workflow for designers
/// </summary>
[DisallowMultipleComponent]
public abstract class TriggerReactionParent : SuperBehaviour {

}
