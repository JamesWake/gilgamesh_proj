﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Collider))]
public class TriggerTriggerCollider : Trigger
{
    [Header("Fire on both enter and exit?")]
    [Tooltip("With this ticked, the reaction will trigger when an object both enters and exits the trigger zone, otherwise it will only trigger when it enters")]
    public bool enterAndExit = false;

    void Start ()
    {
        if (!GetComponent<Collider>().isTrigger)
        {
            Debug.LogWarning(this.name + " has a TriggerTriggerCollider attached to it, but the collider attached is not set to trigger. Did you mean to set it to be a trigger?", this);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Fire(other.gameObject);
    }

    void OnTriggerExit(Collider other)
    {
        if (enterAndExit) Fire(other.gameObject);
    }
}
