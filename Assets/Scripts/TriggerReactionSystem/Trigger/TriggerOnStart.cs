﻿using UnityEngine;
using System.Collections;
/// <summary>
/// Fires on start -James
/// </summary>
public class TriggerOnStart : Trigger {
    
	void Start () {
        Fire(gameObject);
	}
}
